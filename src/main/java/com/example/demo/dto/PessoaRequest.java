package com.example.demo.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import lombok.Data;

import java.time.LocalDateTime;

@Data
@JsonPropertyOrder({ "nome","dataNascimento","endereco","logradouro","cep","numero","cidade","estado"})
public class PessoaRequest {
    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty("id")
    private Integer id;
    @JsonProperty("id")
    private String nome;
    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty("id")
    private LocalDateTime dataNascimento;
    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty("id")
    private String endereco;
    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty("id")
    private String logradouro;
    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty("id")
    private String cep;
    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty("id")
    private Integer numero;
    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty("id")
    private String cidade;
    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty("id")
    private String estado;
}
