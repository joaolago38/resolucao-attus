package com.example.demo.dto;

import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonPropertyOrder({ "nome","dataNascimento","endereco","logradouro","cep","numero","cidade","estado"})
public class PessoaResponse extends PessoaRequest{
}
