package com.example.demo.model;

import lombok.Builder;
import lombok.Data;

import javax.persistence.*;
import java.time.LocalDateTime;
@Entity
@Data
@Builder
@Table(name = "pessoa")
public class Pessoa {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private Integer id;
    @Column(name = "nome", nullable = false)
    private String nome;
    @Column(name = "dataNascimento", nullable = false)
    private LocalDateTime dataNascimento;
    @Column(name = "endereco", nullable = false)
    private String endereco;
    @Column(name = "logradouro", nullable = false)
    private String logradouro;
    @Column(name = "cep", nullable = false)
    private String cep;
    @Column(name = "numero", nullable = false)
    private Integer numero;
    @Column(name = "cidade", nullable = false)
    private String cidade;
    @Column(name = "estado", nullable = false)
    private String estado;

    public Pessoa(Integer id, String nome, LocalDateTime dataNascimento, String endereco, String logradouro, String cep, Integer numero, String cidade, String estado) {
        this.id = id;
        this.nome = nome;
        this.dataNascimento = dataNascimento;
        this.endereco = endereco;
        this.logradouro = logradouro;
        this.cep = cep;
        this.numero = numero;
        this.cidade = cidade;
        this.estado = estado;
    }

    public Pessoa(){

    }
}
