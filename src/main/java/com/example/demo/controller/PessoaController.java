package com.example.demo.controller;

import com.example.demo.model.Pessoa;
import com.example.demo.service.PessoaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
public class PessoaController {
    @Autowired
    private PessoaService pessoaService;

    // Save operation
    @PostMapping("/pessoa")
    public Pessoa savePessoa(@Valid @RequestBody Pessoa pessoa) {
        return pessoaService.salvaPessoa(pessoa);
    }

    // Read operation
    @GetMapping("/pessoa")
    public List<Pessoa> buscaPessoaList() {
        return pessoaService.buscaPessoaList();
    }

    // Update operation
    @PutMapping("/pessoa")
    public Pessoa
    updatePessoa(@RequestBody Pessoa pessoa) {
        return pessoaService.atualizaPessoa(pessoa);

    }
}
