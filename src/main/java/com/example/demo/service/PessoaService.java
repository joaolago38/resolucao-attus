package com.example.demo.service;

import com.example.demo.model.Pessoa;

import java.util.List;
import java.util.Optional;

public interface PessoaService {
    Pessoa salvaPessoa(Pessoa pessoa);


    List<Pessoa> buscaPessoaList();

    Optional<Pessoa> buscaPorId(Integer id);

    // Update operation
    Pessoa atualizaPessoa(Pessoa pessoa);

    // Delete operation
    void deletaPessoaById(Integer pessoaId);
}
