package com.example.demo.service.impl;

import com.example.demo.model.Pessoa;
import com.example.demo.repository.PessoaRepository;
import com.example.demo.service.PessoaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;


@Service
public class PessoaServiceImpl implements PessoaService {
    @Autowired
    private PessoaRepository pessoaRepository;

    @Override
    public Pessoa salvaPessoa(Pessoa pessoa) {
        return pessoaRepository.save(pessoa);
    }

    @Override
    public List<Pessoa> buscaPessoaList() {
        return (List<Pessoa>)
                pessoaRepository.findAll();
    }

    @Override
    public Optional<Pessoa> buscaPorId(Integer id) {
        return pessoaRepository.findById(id);
    }

    @Override
    public Pessoa atualizaPessoa(Pessoa pessoa) {
        return pessoaRepository.save(pessoa);
    }

    @Override
    public void deletaPessoaById(Integer pessoaId) {
        pessoaRepository.deleteById(pessoaId);
    }
}
