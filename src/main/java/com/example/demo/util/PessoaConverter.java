package com.example.demo.util;

import com.example.demo.dto.PessoaRequest;
import com.example.demo.dto.PessoaResponse;
import com.example.demo.model.Pessoa;

public class PessoaConverter {
    public static PessoaResponse toPessoaResponse(Pessoa pessoa) {

        PessoaResponse pessoaResponse = new PessoaResponse();

        pessoaResponse.setId(pessoa.getId());
        pessoaResponse.setNome(pessoa.getNome());
        pessoaResponse.setCidade(pessoa.getCidade());
        pessoaResponse.setEndereco(pessoa.getEndereco());
        pessoaResponse.setDataNascimento(pessoa.getDataNascimento());
        pessoaResponse.setNumero(pessoa.getNumero());
        pessoaResponse.setEstado(pessoa.getEstado());
        pessoaResponse.setLogradouro(pessoa.getLogradouro());
        return pessoaResponse;
}
    public static PessoaRequest toPessoaRequest(Pessoa pessoa) {
        PessoaRequest pessoaRequest = new PessoaRequest();
        pessoaRequest.setId(pessoa.getId());
        pessoaRequest.setNome(pessoa.getNome());
        pessoaRequest.setCidade(pessoa.getCidade());
        pessoaRequest.setEndereco(pessoa.getEndereco());
        pessoaRequest.setDataNascimento(pessoa.getDataNascimento());
        pessoaRequest.setNumero(pessoa.getNumero());
        pessoaRequest.setEstado(pessoa.getEstado());
        pessoaRequest.setLogradouro(pessoa.getLogradouro());
        return pessoaRequest;
    }
    public static Pessoa toPessoa(PessoaRequest pessoaRequest) {
        Pessoa pessoa = new Pessoa();

        pessoa.setId(pessoa.getId());
        pessoa.setLogradouro(pessoa.getLogradouro());
        pessoa.setNome(pessoa.getNome());
        pessoa.setCidade(pessoa.getCidade());
        pessoa.setEndereco(pessoa.getEndereco());
        pessoa.setDataNascimento(pessoa.getDataNascimento());
        pessoa.setNumero(pessoa.getNumero());
        pessoa.setEstado(pessoa.getEstado());
        return pessoa;
    }
}
